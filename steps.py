import RPi.GPIO as GPIO
from time import sleep

class Steps:
    A = 0
    B = 0
    C = 0
    D = 0
    time = 0.002
    def __init__(self, A, B, C, D):
        self.A = A
        self.B = B
        self.C = C
        self.D = D

    def step1(self):
        GPIO.output(self.D, True)
        sleep(self.time)
        GPIO.output(self.D, False)

    def step2(self):
        GPIO.output(self.D, True)
        GPIO.output(self.C, True)
        sleep(self.time)
        GPIO.output(self.D, False)
        GPIO.output(self.C, False)

    def step3(self):
        GPIO.output(self.C, True)
        sleep(self.time)
        GPIO.output(self.C, False)

    def step4(self):
        GPIO.output(self.B, True)
        GPIO.output(self.C, True)
        sleep(self.time)
        GPIO.output(self.B, False)
        GPIO.output(self.C, False)

    def step5(self):
        GPIO.output(self.B, True)
        sleep(self.time)
        GPIO.output(self.B, False)

    def step6(self):
        GPIO.output(self.A, True)
        GPIO.output(self.B, True)
        sleep(self.time)
        GPIO.output(self.A, False)
        GPIO.output(self.B, False)

    def step7(self):
        GPIO.output(self.A, True)
        sleep(self.time)
        GPIO.output(self.A, False)

    def step8(self):
        GPIO.output(self.D, True)
        GPIO.output(self.A, True)
        sleep(self.time)
        GPIO.output(self.D, False)
        GPIO.output(self.A, False)

    def turn_left(self):
        self.step8()
        self.step7()
        self.step6()
        self.step5()
        self.step4()
        self.step3()
        self.step2()
        self.step1()

    def turn_right(self):
        self.step1()
        self.step2()
        self.step3()
        self.step4()
        self.step5()
        self.step6()
        self.step7()
        self.step8()
