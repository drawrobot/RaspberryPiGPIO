# RaspberryPi GPIO
This project consists of 3 different python files.

**steps.py**
This file is responsible for ordering the motors to go a single step forward or backward.

**move.py**
move.py is responsible for reading all the commands from a given file ( .txt) instructing the motors on how to execute them. move.py uses steps.py for controlling the motors.

**client.py**
client.py is responsible for the TCP Connection to our server and receiving the commands from the server. After the commands were successfully received move.py is started and thus, the drawing begins.
