import socket
import select
import errno
  
TCP_IP = '172.16.4.37'
TCP_PORT = 1521
BUFFER_SIZE = 1024
PASSWORD = '74feceabeb2da94eca5eba0cd11a713723adab589ba9778898768527c930967e'
file = open("data.txt", "w")

def send(socket, msg):
    socket.send(msg+'\n\r')

socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
socket.connect((TCP_IP, TCP_PORT))

send(socket, PASSWORD)

socket.setblocking(0)

run_main_loop = True
while run_main_loop:
    # Wait for events...
    read_ready, _, _ = select.select([socket], [], [])

    if socket in read_ready:
        # The socket have data ready to be received
        buffer = ''
        continue_recv = True

        while continue_recv:
            try:
                # Try to receive som data
                buffer += socket.recv(1024)
            except:
                if e.errno != errno.EWOULDBLOCK:
                    # Error! Print it and tell main loop to stop
                    print 'Error: %r' % e
                    run_main_loop = False
                # If e.errno is errno.EWOULDBLOCK, then no more data
                continue_recv = False

    # We now have all data we can in "buffer"
    #print '%r' % buffer
    file.write(buffer)

socket.close()
print 'Done'