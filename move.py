from time import sleep
import RPi.GPIO as GPIO
from steps import Steps

def read(file):
    chars = open(file, "r").read().split("\n")
    x = []
    for char in chars:
        if char.strip("\r") != '':
            x.append(int(char.strip("\r")))

    return x

GPIO.setmode(GPIO.BCM)
#PINS
A = 18
B = 23
C = 24
D = 25
E = 4
F = 17
G = 27
H = 22


GPIO.setup(A, GPIO.OUT)
GPIO.setup(B, GPIO.OUT)
GPIO.setup(C, GPIO.OUT)
GPIO.setup(D, GPIO.OUT)
GPIO.output(A, False)
GPIO.output(B, False)
GPIO.output(C, False)
GPIO.output(D, False)

GPIO.setup(E, GPIO.OUT)
GPIO.setup(F, GPIO.OUT)
GPIO.setup(G, GPIO.OUT)
GPIO.setup(H, GPIO.OUT)
GPIO.output(E, False)
GPIO.output(F, False)
GPIO.output(G, False)
GPIO.output(H, False)

commands = read("tmp.txt")

for command in commands:
    if command == 0:
        move_first_left()
    elif command == 1:
        move_first_right()
    elif command == 2:
        move_sec_left()
    elif command == 3:
        move_sec_right()
